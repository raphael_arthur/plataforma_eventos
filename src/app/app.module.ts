import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { QRCodeModule } from 'angular2-qrcode';


//pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SignUpPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { Menu } from '../pages/menu/menu';
import { CreateEvent } from '../pages/create-event/create-event';
import { EditEvent } from '../pages/edit-event/edit-event';
import { EventPage } from '../pages/event-page/event-page';
import { EventRegistration } from '../pages/event-registration/event-registration';
//---------------------

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Data } from '../providers/data';



//Feature Module
import { CoreModule } from './core/core.module';

// AppRoutingModule
//import { AppRoutingModule } from './AppRoutingModule.module';

import { UserService } from '../services/user.service';
import { EventService } from '../services/event.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SignUpPage,
    LoginPage,
    Menu,
    CreateEvent,
    EditEvent,
    EventPage,
    EventRegistration,
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CoreModule,
    HttpModule,
    QRCodeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SignUpPage,
    LoginPage,
    Menu,
    CreateEvent,
    EditEvent,
    EventPage,
    EventRegistration
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Data,
    UserService,
    EventService,
    BarcodeScanner
  ]
})
export class AppModule {}
