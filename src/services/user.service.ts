import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class UserService extends ServerService {
    constructor(http:Http){
        super(http);
    }

    createUser(formData:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'login=' + formData['login'] + '&password=' + formData['password'] + '&firstName=' + 
                formData['firstName'] + '&email=' + formData['email'];
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_eventos/request/signup.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }
}
