import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventService } from '../../services/event.service';
import { Observable, Observer } from 'rxjs/Rx';

import { EditEvent } from '../edit-event/edit-event';
 
/*Página Menu será a página principal para o usuário que esteja logado e conterá as seguintes informações:
  -> Lista de eventos do usuário
  Menu:
    ->  Cadastrar usuário [caso admin]
    ->  Editar Perfil
    ->  Criar evento 
    ->  Estatísticas [no futuro]

*/

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class Menu {
  private events_observer:any;
  private events:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private eventService:EventService) 
  {
    /*this.events = Observable.create( observer =>{
      this.events_observer = observer;
    });
*/
    this.getEvents();  
    console.log("estou construtor Menu");
    
  }

  private getEvents():void
  {
    this.events_observer = this.eventService.getEvents(sessionStorage.getItem("userID")).subscribe( data => {
        console.log("dentro do subscribe");
        this.events = this.eventService.events_found;
    });
    
  }

  private editEvent(event:any):void
  {
    this.navCtrl.push(EditEvent, { event: event } );
  }

  private delEvent(id_event):void
  {
    this.eventService.delEvent(id_event).subscribe( data => {
        console.log("dentro do subscribe");
        this.events = this.eventService.events_found;
    });
  }

  private createEvent():void
  {

  }

}
