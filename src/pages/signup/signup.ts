import { Component } from '@angular/core';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { NavController, NavParams } from 'ionic-angular';

@Component({
    selector: 'sign-up-page',
    templateUrl: 'signup.html'
})
export class SignUpPage{
    private signup:FormGroup;

    constructor( private formBuilder: FormBuilder, private userService:UserService ){
        this.signup = this.formBuilder.group({
            login: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            firstName: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            email:[null, Validators.required]
            
        });
        
            console.log("estou signup")
            console.log(sessionStorage.getItem('userID'));
    }
    public sendData(){
        console.log("send_Data singup")
        console.log(this.signup.value);
        this.userService.createUser(this.signup.value).subscribe( data => {
            //console.log("dentro do subscribe");
        });
        console.log(this.signup.value);
    }
}

