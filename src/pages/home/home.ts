import { Component, NgModule, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { EventService } from '../../services/event.service';
import { EventPage } from '../event-page/event-page';
import { QRCodeModule } from 'angular2-qrcode';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CreateEvent } from '../create-event/create-event';
//import { Observable, Observer } from 'rxjs/Rx';
//---------------------------------------------
import { LoginPage } from '../login/login';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private events_observer:any;
  private events:any;
  public hasEvent:boolean;
  constructor(public navCtrl: NavController, private eventService:EventService, public navParams: NavParams, private barcodeScanner: BarcodeScanner) 
  {
    this.hasEvent = false;
    this.getAllEvents();
  }


  private getAllEvents():void
  {
    this.events_observer = this.eventService.getAllEvents().subscribe( data => {
        console.log("dentro do subscribe");
        this.events = this.eventService.events_found;
    });
    if(this.events != "")
    {
      this.hasEvent = true;
    }
  }

  private goLogin():void
  {
    this.navCtrl.push(LoginPage);
  }

  private goEventPage(event:any):void
  {
    this.navCtrl.push(EventPage, {event:event});
  }

  private Scan():void
  { 
    this.barcodeScanner.scan().then((barcodeData) => {
      this.goLogin();
    }, (err) => {
      this.navCtrl.push(CreateEvent);
    });

  }
}
