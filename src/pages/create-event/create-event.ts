import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EventService } from '../../services/event.service'; 



@IonicPage()
@Component({
  selector: 'page-create-event',
  templateUrl: 'create-event.html',
})
export class CreateEvent {
  private create_event:FormGroup;

  constructor(private eventService:EventService, private formBuilder:FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.create_event = this.formBuilder.group({
      event_name: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9 ]*')])],
      begin_date: [null, Validators.required],
      end_date: [null, Validators.required],
      adress:[null, Validators.required],
      place:[null, Validators.required],
      event_description:[null, Validators.required]
    });
    console.log("Estou create_event");

  }

  public sendData(){
    console.log("send_Data create_event")
    console.log(this.create_event.value);
    this.eventService.createEvent(this.create_event.value).subscribe( data => {
        console.log("dentro do subscribe");
    });
    console.log(this.create_event.value);
  }

}
