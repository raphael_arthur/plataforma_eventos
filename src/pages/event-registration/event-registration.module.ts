import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventRegistration } from './event-registration';

@NgModule({
  declarations: [
    EventRegistration,
  ],
  imports: [
    IonicPageModule.forChild(EventRegistration),
  ],
  exports: [
    EventRegistration
  ]
})
export class EventRegistrationModule {}
