import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventService } from '../../services/event.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-event-registration',
  templateUrl: 'event-registration.html',
})
export class EventRegistration {

  public event:any;
  private register:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private eventService:EventService, private formBuilder:FormBuilder) 
  {
    this.event = navParams.get("event");

    this.register = this.formBuilder.group({
        firstName: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
        lastName: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
        email:[null, Validators.compose([Validators.required])],
        birthday:[null, Validators.required],
        matricula:[null, Validators.pattern('[0-9]*')],
        cpf:[null, Validators.compose([Validators.required, Validators.pattern('[0-9]*')])],

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventRegistration');
  }

  public sendData(){
    console.log("send_Data create_event")
    console.log(this.register.value);
    this.eventService.registerEvent(this.register.value, this.event.idevento).subscribe( data => {
        console.log("dentro do subscribe");
    });
    console.log(this.register.value);
  }
}
