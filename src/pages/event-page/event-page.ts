import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventRegistration } from '../event-registration/event-registration';
/**
 * Generated class for the EventPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-event-page',
  templateUrl: 'event-page.html',
})
export class EventPage {

  public event:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    this.event = navParams.get("event");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventPage');
  }

  goRegistration(event):void
  {
    this.navCtrl.push(EventRegistration, {event:event});
  }
}
