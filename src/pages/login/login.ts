import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../app/core/services/auth.service';
//import { Router } from '@angular/router';
import { AlertController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { Menu } from '../menu/menu';

@Component({
    selector: 'login-page',
    templateUrl: 'login.html'
})
export class LoginPage{

    private login : FormGroup;

    constructor( private alertCtrl:AlertController, private formBuilder: FormBuilder, private auth: AuthService, public navCtrl: NavController, public navParams: NavParams  /*private router: Router*/){
        //construct the FormBuilder to login forms
        this.login = this.formBuilder.group({
            email: [null, Validators.required],
            password: [null, Validators.required],
        });
    }

    public sendData():void{
        this.auth.login(this.login.value.email, this.login.value.password).subscribe(data => {
            let token = sessionStorage.getItem('userToken');
            console.log(sessionStorage);
            if(sessionStorage.getItem('userBlocked') == 'true'){
                this.presentAlert();
                //this.router.navigate(['/home']);
            } else if(sessionStorage.getItem('userToken') != "" ){
                console.log("TENHO TOKEN");
                this.navCtrl.push(Menu);
            }
            console.log(data);
        });
    }

    private presentAlert():void {
        let alert = this.alertCtrl.create({
            title: 'Conta Bloqueada',
            subTitle: 'Por questões de segurança sua conta foi bloqueada. Por favor, envie um email para comunicacao@cdt.unb.br.',
            buttons: ['Entendi']
        });
        alert.present();
    }
}
