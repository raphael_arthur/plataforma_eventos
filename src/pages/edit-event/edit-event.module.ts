import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditEvent } from './edit-event';

@NgModule({
  declarations: [
    EditEvent,
  ],
  imports: [
    IonicPageModule.forChild(EditEvent),
  ],
  exports: [
    EditEvent
  ]
})
export class EditEventModule {}
