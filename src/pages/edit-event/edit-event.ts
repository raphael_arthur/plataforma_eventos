import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EventService } from '../../services/event.service'; 
/**
 * Generated class for the EditEvent page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-edit-event',
  templateUrl: 'edit-event.html',
})
export class EditEvent {

  event:any;
  constructor(private eventService:EventService, private formBuilder:FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.event = navParams.get('event');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditEvent');
  }

  editForm()
  {
    this.eventService.editEvent(this.event).subscribe( data => {
        console.log("dentro do subscribe");
    });
  }

}
